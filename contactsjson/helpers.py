# -*- coding: utf-8 -*-
'''
@brief common helper functions
@file helpers.py
@package contactjson
'''

import yaml

def yaml_load(file):
    ''' handles reading a YAML file '''
    with open(file, 'r') as f:
        config = yaml.load(f)
    return config
