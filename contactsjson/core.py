# -*- coding: utf-8 -*-
'''
@brief argument parsing and log initialization
@file core.py
@package contactjson
'''

import logging
import argparse
from helpers import yaml_load


def parse_args():
    '''
    inits and runs the argparse module
    Returns the parsed arguments
    '''
    parser = argparse.ArgumentParser(
        description="Converts CSV files to JSON", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument("-i", "--input", metavar="FILE",
                        help="CSV file(s) to read in", required=True, nargs="+")
    required_named.add_argument("-o", "--output", metavar="FILE",
                        help="output JSON files", required=True)
    
    logging_arguments = parser.add_argument_group('logging arguments')
    logging_arguments.add_argument("-l", "--log-config", metavar="FILE",
                        help="specify the logger configuration YAML file", default="logger.yml")
    logging_arguments.add_argument(
        "-d", "--debug", help="force to debug log level", action="store_true")
    
    parser.add_argument("-x", "--index-column", metavar="NAME",
                        help="specify the index column name", default="id")
    parser.add_argument("-r", "--drop-index-column",
                        help="drop the index column when writing the JSON file", action="store_true", default=False)
    parser.add_argument("-n", "--no-sanitize",
                        help="turn off row sanitization", action="store_true", default=False)
    return parser.parse_args()


def init_logging(config_file="logger.yml", debug_override=False):
    '''
    initializes the logger with provided config file
    '''
    import logging.config
    logging_config = {}
    io_error_message = None

    try:
        # first try to load the logger config from a file
        logging_config = yaml_load(config_file)
    except IOError as e:
        # file read error, setup basic console logger with debug level
        # will warn the user later if this happens
        io_error_message = e.strerror

        logging_config = dict(
            version=1,
            formatters={
                'default': {'format': '%(asctime)s %(name)-12s %(levelname)-8s %(message)s', 'datefmt': '%Y-%m-%d %H:%M:%S'},
            },
            handlers={
                'standard': {'class': 'logging.StreamHandler',
                             'formatter': 'default',
                             'level': logging.DEBUG,
                             'stream': 'ext://sys.stdout'},
            },
            root={
                'handlers': ['standard'],
                'level': logging.DEBUG,
            },
        )

    if debug_override:
        logging_config['root']['level'] = "DEBUG"

    # setup logging
    logging.config.dictConfig(logging_config)
    log = logging.getLogger("init_logging")

    if io_error_message:
        # let the user know that the file was not found, but keep going with
        # defaults
        log.warn(
            "unable to load from config file, using default logging config: %s", io_error_message)
    else:
        log.debug("succesfully loaded: %s", config_file)
