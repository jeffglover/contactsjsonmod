# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

setup(
    name='contactsjson',
    version='0.0.1',
    description='Converts contacts CSV file to JSON',
    long_description=readme,
    author='Jeff Glover',
    author_email='js.glover@gmail.com',
    url='https://gitlab.com/jeffglover/contactsjsonmod',
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=[
          'pandas',
      ],
    scripts=['csv_to_json']
)
