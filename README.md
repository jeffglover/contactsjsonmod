# Contacts (from CSV) to JSON
## Dependencies
Tested with the following libraries on Arch Linux as of September 3, 2016
* Python 2.7.12 (older versions of Python 2.7 will most likely work)
* Pandas 0.18.1 (handles both CSV and JSON parsing)
* PyYAML 3.12
* other Python modules used: logging, argparse, re, unittest, StringIO

## Docker
My Arch Linux Docker image is publicly available on [DockerHub](https://hub.docker.com/r/jeffglover/archlinux/tags/)
```bash
docker run -t -i --rm jeffglover/archlinux:python-2016-09-03 /bin/bash
pip2 install pandas==0.18.1
pip2 install PyYAML==3.12
```

## Installing (optional)
> Alternatively one may run it from the cloned repo without installing

```bash
python2 setup.py install
```

## Unit Tests
```bash
python2 -m unittest discover -v
```

## Usage
> Required arguments highlighted

<pre>
usage: csv_to_json [-h] -i FILE [FILE ...] -o FILE [-l FILE] [-d] [-x NAME]
                   [-r] [-n]

Converts CSV files to JSON

optional arguments:
  -h, --help            show this help message and exit
  -x NAME, --index-column NAME
                        specify the index column name (default: id)
  -r, --drop-index-column
                        drop the index column when writing the JSON file
                        (default: False)
  -n, --no-sanitize     turn off row sanitization (default: False)

<b>required named arguments:
  -i FILE [FILE ...], --input FILE [FILE ...]
                        CSV file(s) to read in (default: None)
  -o FILE, --output FILE
                        output JSON files (default: None)</b>

logging arguments:
  -l FILE, --log-config FILE
                        specify the logger configuration YAML file (default:
                        logger.yml)
  -d, --debug           force to debug log level (default: False)
</pre>

## Example usage
```
./csv_to_json --debug  --input *.csv --output /tmp/db_import.json
```
