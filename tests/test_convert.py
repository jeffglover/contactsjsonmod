# -*- coding: utf-8 -*-

import unittest
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO

import contactsjson.convert
import contactsjson.core

class CSVtoJSON(unittest.TestCase):
    def setUp(self):
        contactsjson.core.init_logging() # force fault logging config
        self.input_1_csv = StringIO("""id,first_name,last_name,email
1,Ashley,Mitchell,amitchell0@chronoengine.com
2,Lois,Stanley,lstanley1@netvibes.com
3,Sean,Brown,sbrown2@google.es
""")
        self.input_2_csv = StringIO("""id,first_name,last_name,email
4,Jane,Fox,jfox3@nasa.gov
5,Thomas,Shaw,invalid
6,Joseph,Taylor,jtaylor5@technorati.com
""")
        
        self.input_duplicates_csv = StringIO("""id,first_name,last_name,email
4,Jane,Fox,jfox3@nasa.gov
4,Jane,Fox,jfox3@nasa.gov
4,Jane,Fox,jfox3@nasa.gov
""")

    def tearDown(self):
        # Called after the last testfunction was executed
        pass

    def test_ReadCSV(self):
        # create converter with bare minimum for construction
        converter = contactsjson.convert.CSVtoJSON(None, None, "id", False, True)
        # just testing the read_csv function
        df = converter.read_csv(self.input_1_csv)
        
        # this buffer will get filled out by pandas
        output_csv = StringIO()
        df.to_csv(output_csv)
        
        # test!
        self.assertEqual(self.input_1_csv.getvalue(), output_csv.getvalue())
        
    def test_ReadCSVEmailValidation(self):
        expected = """id,first_name,last_name,email
4,Jane,Fox,jfox3@nasa.gov
6,Joseph,Taylor,jtaylor5@technorati.com
"""
        # create converter with bare minimum for construction
        converter = contactsjson.convert.CSVtoJSON(None, None, "id", True, True)
        # just testing the read_csv function
        df = converter.read_csv(self.input_2_csv)
        
        # this buffer will get filled out by pandas
        output_csv = StringIO()
        df.to_csv(output_csv)
        
        # test!
        self.assertEqual(expected, output_csv.getvalue())
        
    def test_toJson(self):
        expected_output = '[{"id":1,"first_name":"Ashley","last_name":"Mitchell","email":"amitchell0@chronoengine.com"},{"id":2,"first_name":"Lois","last_name":"Stanley","email":"lstanley1@netvibes.com"},{"id":3,"first_name":"Sean","last_name":"Brown","email":"sbrown2@google.es"},{"id":4,"first_name":"Jane","last_name":"Fox","email":"jfox3@nasa.gov"},{"id":6,"first_name":"Joseph","last_name":"Taylor","email":"jtaylor5@technorati.com"}]'
        
        json_output_buffer = StringIO()
        input_files = [ self.input_1_csv, self.input_2_csv ]
        
        converter = contactsjson.convert.CSVtoJSON(input_files, json_output_buffer, "id", True, True)
        converter.convert()
        
        self.assertEqual(expected_output, json_output_buffer.getvalue())
        
    def test_toJsonDuplicates(self):
        expected_output = '[{"id":4,"first_name":"Jane","last_name":"Fox","email":"jfox3@nasa.gov"}]'
        json_output_buffer = StringIO()
        input_files = [ self.input_duplicates_csv ]
        
        converter = contactsjson.convert.CSVtoJSON(input_files, json_output_buffer, "id", True, True)
        converter.convert()
        
        self.assertEqual(expected_output, json_output_buffer.getvalue())
      
    def test_toJsonRemoveIdField(self):
        expected_output = '[{"first_name":"Jane","last_name":"Fox","email":"jfox3@nasa.gov"}]'
        json_output_buffer = StringIO()
        input_files = [ self.input_duplicates_csv ]
        
        converter = contactsjson.convert.CSVtoJSON(input_files, json_output_buffer, "id", True, False)
        converter.convert()
        
        self.assertEqual(expected_output, json_output_buffer.getvalue())
        
    def test_toJsonBadOutputFile(self):
        json_invalid_file = "/tmp/invalid_dir_012345/output.json"
        input_files = [ self.input_duplicates_csv ]
        
        converter = contactsjson.convert.CSVtoJSON(input_files, json_invalid_file, "id", True, True)
        with self.assertRaises(IOError):
          converter.convert()
        
class RowSanitizer(unittest.TestCase):
    def setUp(self):
        self.is_odd = lambda x: x%2

    def tearDown(self):
        # Called after the last testfunction was executed
        pass

    def test_RowSanitizer(self):
        row_1 = {'foo':6}
        row_2 = {'foo':9}
        sanitize = contactsjson.convert.RowSanitizer("foo", self.is_odd)
        
        self.assertFalse(sanitize(row_1))
        self.assertTrue(sanitize(row_2))
      
if __name__ == "__main__":
    unittest.main()
